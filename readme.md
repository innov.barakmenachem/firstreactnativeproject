# First React Native Project

This is the first time I used react native.

## ReactNative-Ubuntu-Vscode Installation

1. I Useed Ubuntu 19.04.
2. I installed visual studio and android studio(+ installing of virtual device).
3. I installed node and brew.
4. the Path env look like that:

```
#adding installation folders
export PATH=/snap/bin:$PATH
export ANDROID_HOME=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:/home/barak/Android/Sdk/platform-tools
export PATH=/home/linuxbrew/.linuxbrew/bin:$PATH
export JAVA_HOME=/snap/android-studio/80/android-studio/jre
export PATH=$PATH:/usr/bin/npm:$PATH
```
5. I installed react native as in https://facebook.github.io/react-native/docs/getting-started
6. I run the next commands:
```
> npx react-native init AwesomeProject
> cd AwesomeProject
> npm start
> npx react-native run-android
```
7. to instegrate it to vscode, i installed the extentions of react native.
8. RESTART the PC and try to create configure of launching in vscode "debug on android".
9. press F5, and the app should run :)